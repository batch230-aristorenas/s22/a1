// console.log("Hello World!");


/*
    Create functions which can manipulate our arrays.
*/

let registeredUsers = [

    "James Jeffries",
    "Gunther Smith",
    "Macie West",
    "Michelle Queen",
    "Shane Miguelito",
    "Fernando Dela Cruz",
    "Akiko Yukihime"
];

let friendsList = [];

/*
    
   1. Create a function which will allow us to register into the registeredUsers list.
        - this function should be able to receive a string.
        - determine if the input username already exists in our registeredUsers array.
            -if it is, show an alert window with the following message:
                "Registration failed. Username already exists!"
            -if it is not, add the new username into the registeredUsers array and show an alert:
                "Thank you for registering!"
        - invoke and register a new user.
        - outside the function log the registeredUsers array.

*/
function register(newUser){

	let isExistingUser = registeredUsers.includes(newUser);

	if(isExistingUser === true){
		alert("Registration failed. Username already exists!")
	}
	else{
		registeredUsers.push(newUser);
		alert("Thank you for registering!");
	}

}

// register("Conan O' Brein");
// console.log(registeredUsers);



//     function register(data1, data2){

//     	let toRegister = registeredUsers.push("Conan O' Brien");
//     	register = registeredUsers + toRegister
//     	// let existingUsers = registeredUsers.include("James Jeffries", "Gunther Smith", "Macie West", "Michelle Queen", "Shane Miguelito", "Fernando Dela Cruz", "Akiko Yukihime");

//     }
// register(registeredUsers, toRegister);
/*

let data8 = "Conan O'Brien";
function register(data1, data2, data3, data4, data5, data6, data7){

// console.log(registeredUsers);
let registrationFound = registeredUsers.includes(data8);
if(registrationFound === true){
alert("Registration failed. Username already exists!");
}
if(registrationFound === false){
alert("Thank you for registering!");

}
}
*/
// register("James Jeffries", "Gunther Smith", "Macie West", "Michelle Queen", "Shane Miguelito", "Fernando Dela Cruz", "Akiko Yukihime");

/*
    2. Create a function which will allow us to add a registered user into our friends list.
        - this function should be able to receive a string.
        - determine if the input username exists in our registeredUsers array.
            - if it is, add the foundUser in our friendList array.
                    -Then show an alert with the following message:
                        - "You have added <registeredUser> as a friend!"
            - if it is not, show an alert window with the following message:
                - "User not found."
        - invoke the function and add a registered user in your friendsList.
        - Outside the function log the friendsList array in the console.

*/

function addFriend(newFriend){

	let isExistingFriend = registeredUsers.includes(newFriend);

	if(isExistingFriend === true){
		friendsList.push(newFriend);
		alert("You have added " + newFriend + " as a friend!");
	}
	else{
		alert("User not found.");
	}

}

// addFriend("Akiko Yukihime");
// console.log(friendsList);

	/*

	let toRegister = registeredUsers.push(data8);
	// console.log(registeredUsers);

function findUsers(newFriend){
	let findUsers = registeredUsers.includes("James Jeffries", "Gunther Smith", "Macie West", "Michelle Queen", "Shane Miguelito", "Fernando Dela Cruz", "Akiko Yukihime");
	if(findUsers === true){

		let addedUsers1 = friendsList.push(registeredUsers[7]);
		console.log(friendsList);
		alert("You have added " + registeredUsers[7] + " as a friend!");
	}
	if(findUsers === false){
		alert("User not found.");
	}
}

*/
// findUsers();

/*
    3. Create a function which will allow us to show/display the items in the friendList one by one on our console.
        - If the friendsList is empty show an alert: 
            - "You currently have 0 friends. Add one first."
        - Invoke the function.


*/

	function displayFriends(){
		let featureFriends = friendsList.forEach(function(perElement){
			console.log(perElement);
		});
		if(friendsList == 0){
			alert("You currently have 0 friends. Add one first.");
		}
	}
	displayFriends();
	

    /*function addRegisteredUsers(index){
		let addedUsers2 = friendsList.push(registeredUsers[6]);
		console.log(friendsList);
		alert("You have added " + registeredUsers[6] + " as a friend!");

	}*/

/*
    4. Create a function which will display the amount of registered users in your friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - If the friendsList is not empty show an alert:
            - "You currently have <numberOfFriends> friends."
        - Invoke the function

*/

function displayNumberOfFriends(index){
	let countOfFriends = friendsList.length;
	console.log(countOfFriends);
	if(countOfFriends == 0){
		alert("You currently have 0 friends. Add one first.");
	}
	else{
		alert("You currently have " + friendsList.length + " friends.");
	}
}
// displayNumberOfFriends();
/*function countFriendList(index){
let friendsListLength = friendsList.length;

	if(friendsList.length === 0){
		alert("You currently have 0 friends. Add one first");
	}

	else{
		alert("You currently have " + friendsList.length + " friends.");
	}

}*/
// countFriendList();

/*
    5. Create a function which will delete the last registeredUser you have added in the friendsList.
        - If the friendsList is empty show an alert:
            - "You currently have 0 friends. Add one first."
        - Invoke the function.
        - Outside the function log the friendsList array.

*/
function deleteFriend(unfriend){
	let eraseFriend = friendsList.pop(unfriend);
	if(friendsList == 0){
		alert("You currently have 0 friends. Add one first.");
	}
}
// deleteFriend("Akiko Yukihime");


/*function eraseUser(index){
	let chooseErasedUser = friendsList.pop();
	if(friendsList.length === 0){
		alert("You cuurently have 0 friends. Add one first");
	}
	else{
		console.log(friendsList);
	}
}
eraseUser();
*/

/*
    Stretch Goal:

    Instead of only deleting the last registered user in the friendsList delete a specific user instead.
        -You may get the user's index.
        -Then delete the specific user with splice().

*/



